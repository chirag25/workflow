import json
import argparse
import cv2
import pandas as pd
import hashlib
import uuid
import csv
from copy import deepcopy
import requests
from block_timer.timer import Timer as BlockTimer
from io import BytesIO
import imagehash
import time
import traceback
from PIL import Image


class image:
    def compute_hash(self, image, hash_size=16):
        url_info = dict()
        if image.startswith(('https://', 'http://', 'ftp://', 'localhost')):
            try:
                time_dict = dict()
                with BlockTimer(time_dict, 'image_download & bytes conversion'):
                    download_res = requests.get(image)
                    bin_obj = BytesIO(download_res.content)
                with BlockTimer(time_dict, 'converting_to_image'):
                    img = Image.open(deepcopy(bin_obj))
                with BlockTimer(time_dict, 'hash_calculation'):
                    ratio = int((min(img.size) * 1.0) / hash_size)
                    hash_val = str(imagehash.phash(img, hash_size=hash_size, highfreq_factor=ratio))

                '''
                with BlockTimer(time_dict, 's3_upload'):
                    image_name = str(uuid.uuid5(uuid.NAMESPACE_URL, hash_val + '_' + str(img.size[1]) + '_' + str(img.size[0]) + '_' + str(img.mode)))
                    s3_upload(bin_obj, s3_bucket,
                              os.path.join(s3_image_folder, image_name + '.jpg'),
                              acl=s3_acl_public,
                              content_type=s3_image_content_type)
                '''

                url_info[image] = dict(
                    success=True,
                    hash=hash_val,
                    height=img.size[1],
                    # s3_path=os.path.join(s3_base_url, s3_image_folder, image_name + '.jpg'),
                    width=img.size[0],
                    image_mode=img.mode,
                    time_dict=time_dict,
                    timestamp=time.time())
            except Exception as e:
                print(e)
                url_info[image] = dict(
                    success=False,
                    reason=str(e),
                    traceback=str(traceback.print_exc()),
                    timestamp=time.time())
            finally:
                return url_info[image]
        else:
            try:
                img = Image.open(image)
                ratio = int((min(img.size) * 1.0) / hash_size)
                hash_val = str(imagehash.phash(img, hash_size=hash_size, highfreq_factor=ratio))
                url_info[image] = dict(
                    success=True,
                    hash=hash_val,
                    height=img.size[1],
                    # s3_path=os.path.join(s3_base_url, s3_image_folder, image_name + '.jpg'),
                    width=img.size[0],
                    image_mode=img.mode,
                    timestamp=time.time())

            except Exception as e:
                print("Corrupt Image")
                print(e)
                url_info[image] = dict(
                    success=False,
                    reason=str(e),
                    traceback=str(traceback.print_exc()),
                    timestamp=time.time())
            finally:
                return url_info[image]


class text:
    def compute_hash(self, text):
        if text:
            return uuid.uuid5(uuid.NAMESPACE_URL, text)
        else:
            return "123456789"


class Workflow:
    def __init__(self, pth1, pth2=None):
        if pth2 is None:
            self.config = json.load(open(pth1,))
        else:
            self.config_1 = json.load(open(pth1,))
            self.config_2 = json.load(open(pth2,))

    def compute_file_hash(self, filename, df_lis):

        # Logic for calculating file_hash
        mapping = {}
        for i in df_lis[0].keys():
            mapping[i] = [j for j in self.config.keys() for k in range(len(self.config[j])) if (self.config[j][k]['column_name'] == i and j != 'metadata')]
        content_hash_title = [k+'_hash' for k, v in mapping.items() if v if v[0] == 'content']
        annotations_hash_title = [k+'_hash' for k, v in mapping.items() if v if v[0] == 'annotations']
        content_hash_list = [str(dict[key]) for dict in df_lis for key in dict.keys() if key in content_hash_title]
        annotations_hash_list = [str(dict[key]) for dict in df_lis for key in dict.keys() if key in annotations_hash_title]
        content_hash_list.sort()
        annotations_hash_list.sort()
        file_hash = uuid.uuid5(uuid.NAMESPACE_URL, (str(uuid.uuid5(uuid.NAMESPACE_URL, str(content_hash_list))) + str(uuid.uuid5(uuid.NAMESPACE_URL, str(annotations_hash_list)))))
        for dict in df_lis:
            dict['file_hash'] = file_hash
        '''
        # Old method for computing 'file_hash'
        with open('Processed_'+filename, 'w') as output_file:
            dict_writer = csv.DictWriter(output_file, df_lis[0].keys(), lineterminator='\n')
            dict_writer.writeheader()
            dict_writer.writerows(df_lis)

        with open('Processed_'+filename, 'r') as f:
            fb = f.read()
            file_hash = uuid.uuid5(uuid.NAMESPACE_URL, fb)
        '''
        # Saving file_hash in the Processed_ file with column name file_hash
        with open('Processed_'+filename, 'w') as output_file:
            dict_writer = csv.DictWriter(output_file, df_lis[0].keys(), lineterminator='\n')
            dict_writer.writeheader()
            dict_writer.writerows(df_lis)
        return file_hash

    def processing(self, df_lis):
        # processing function computes all kinds of hashes except 'file_hash', it also detects duplicate entries in the input
        mapping = {}
        for i in df_lis[0].keys():
            mapping[i] = [self.config[j][k]['type'] for j in self.config.keys() for k in range(len(self.config[j])) if (self.config[j][k]['column_name'] == i and j != 'metadata')]
        # Compute hashes for Content and Annotations
        for count, i in enumerate(df_lis):
            keys = deepcopy(list(i.keys()))
            for j in keys:
                if mapping[j] != []:
                    if (mapping[j][0] == 'image'):
                        img = image()
                        img_info = getattr(img, 'compute_hash')(i[j])
                        if img_info['success'] is True:
                            df_lis[count][str(j)+'_hash'] = img_info['hash']
                        else:
                            df_lis[count][str(j)+'_hash'] = "123456789"
                    else:
                        txt = text()
                        df_lis[count][str(j)+'_hash'] = getattr(txt, 'compute_hash')(i[j])
        # Compute a unique hash for every datapoint
        for count, i in enumerate(df_lis):
            keys = deepcopy(list(i.keys()))
            for j in keys:
                temp_concat = ""
                temp_content_concat = ""
                if j.endswith('_hash'):
                    temp_concat += str(i[j])
                for k in self.config["content"]:
                    for key, value in k.items():
                        if key == 'column_name':
                            temp_content_concat += str(i[value+'_hash'])
            txt = text()
            df_lis[count]['unique_hash'] = getattr(txt, 'compute_hash')(temp_concat)
            df_lis[count]['combined_content_hash'] = getattr(txt, 'compute_hash')(temp_content_concat)
        # Find and Display duplicate entries
        unique_list = [dict['unique_hash'] for dict in df_lis]
        seen = set()
        not_uniq = set()
        for x in unique_list:
            if x not in seen:
                seen.add(x)
            else:
                not_uniq.add(x)
        if not_uniq:
            print("Duplicate entries are present in input. Unique Hashes of these entries are ", list(not_uniq))

        return df_lis

    def rofl(self, df_lis_1, df_lis_2):
        # Check for addition and deletion
        for content in self.config_1['content']:
            for k, v in content.items():
                if k == 'column_name':
                    temp1 = [dict['combined_content_hash'] for dict in df_lis_1 if dict[str(v)+'_hash'] != "123456789"]  # temp1 is list of unique data in file1
                    corrupted1 = len(df_lis_1)-len(temp1)
                    temp2 = [dict['combined_content_hash'] for dict in df_lis_2 if dict[str(v)+'_hash'] != "123456789"]  # temp2 is list of unique data in file2
                    corrupted2 = len(df_lis_2)-len(temp2)
                    print(corrupted1, corrupted2)
                    print(len(temp1), len(temp2))
                    added = list(set(temp2) - set(temp1))
                    deleted = list(set(temp1) - set(temp2))
                    intersection = list(set(temp1).intersection(set(temp2)))
                    print(len(intersection))
                    modified = []
                    for dict1 in df_lis_1:
                        for dict2 in df_lis_2:
                            if ((dict1['combined_content_hash'] == dict2['combined_content_hash']) and (dict1['unique_hash'] != dict2['unique_hash']) and (dict1['combined_content_hash'] in intersection)):  # Check if content hash is same and unique hash is different and lies in intersection
                                for annotation in self.config_1['annotations']:
                                    for key, value in annotation.items():
                                        if key == 'column_name':
                                            if(dict1[str(value)+'_hash'] != dict2[str(value)+'_hash']):
                                                # print(dict1['img_path'], dict2['img_path'])
                                                modified.append({dict1[str(v)+'_hash']: dict1[str(value)+'_hash']})
            break  # No matter how many contents are mentiond in the config, 1 content itself is enough to provide info on addition and deletions
        print("Reached rofl")
        return corrupted1, corrupted2, added, deleted, modified


def check_validity_input(input):
    if isinstance(input, str):
        if input.endswith('.csv'):
            df = pd.read_csv(input)
            df_lis = df.to_dict(orient='records')
        elif input.endswith('.json'):
            df_lis = json.load(open(input))
        else:
            print("input format is string and its not a csv or a json. Unsupported")
            return 0
    elif isinstance(input, (dict, list)):
        df_lis = input
    else:
        print("input format is not a string and its not a csv or a json. Unsupported")
        return 0

    if isinstance(df_lis, dict):
        df_lis = [df_lis]
    return df_lis


def check_validity_config(config1, config2):
    config_1 = json.load(open(config1,))
    config_2 = json.load(open(config2,))
    # Check if there configs are similar
    for i in config_1.keys():
        if i != 'metadata':
            for item_1 in config_1[i]:
                for k1, v1 in item_1.items():
                    flag = 0
                    if config_2[i]:
                        for item_2 in config_2[i]:
                            for k2, v2 in item_2.items():
                                if k1 == k2 and v1 == v2:
                                    flag = 1
                    else:
                        return 0
                    if flag == 0:
                        return 0
    return 1


def main(args):
    if args_dict['type'] == 'indexing':
        df_lis = check_validity_input(args_dict['input'])
        if df_lis != 0:
            workflow = Workflow(args_dict['config'])
            df_lis = workflow.processing(df_lis)
            file_hash = workflow.compute_file_hash(str(args_dict['input']), df_lis)
            print("File Hash for processed csv is ", file_hash)

    if args_dict['type'] == 'diff':
        if check_validity_config(args_dict['f1_config'], args_dict['f2_config']) and check_validity_config(args_dict['f2_config'], args_dict['f1_config']):
            df_lis_1 = check_validity_input(args_dict['f1'])
            df_lis_2 = check_validity_input(args_dict['f2'])
            print(len(df_lis_1))
            if df_lis_1 != 0 and df_lis_2 != 0:
                if not args_dict['f1'].startswith('Processed_'):
                    workflow_1 = Workflow(args_dict['f1_config'])
                    df_lis_1 = workflow_1.processing(df_lis_1)
                    file_hash_1 = workflow_1.compute_file_hash(str(args_dict['f1']), df_lis_1)
                if not args_dict['f2'].startswith('Processed_'):
                    workflow_2 = Workflow(args_dict['f2_config'])
                    df_lis_2 = workflow_2.processing(df_lis_2)
                    file_hash_2 = workflow_2.compute_file_hash(str(args_dict['f2']), df_lis_2)
                if df_lis_1[0]['file_hash'] != df_lis_2[0]['file_hash']:
                    combined_workflow = Workflow(args_dict['f1_config'], args_dict['f2_config'])
                    corrupted1, corrupted2, added, deleted, modified = combined_workflow.rofl(df_lis_1, df_lis_2)
                    print("Number of corrupted images in File 1:", corrupted1)
                    print("Number of corrupted images in File 2:", corrupted2)
                    print("Hashes of images added in File 2:", added)
                    print("Hashes of images deleted from File 1:", deleted)
                    print("{Hash of image: Hash of annotation} where annotations of File 1 were modified in File 2:", modified)
                else:
                    print("Both input files are same")
        else:
            print("Configs are not same")


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--type',
        help='indexing or diff',
        default='diff')
    # INPUTS REQUIRED FOR INDEXING
    parser.add_argument(
        '--input',
        help='path_to_input csv or json',
        default='footweartype_short_order_modified.csv')
    parser.add_argument(
        '--config',
        help='path_to_config json',
        default='config.json')
    # INPUTS REQUIRED FOR DIFF
    parser.add_argument(
        '--f1',
        help='path_to_first_input csv or json',
        default='Processed_footweartype-val.csv')
    parser.add_argument(
        '--f2',
        help='path_to_second_input csv or json',
        default='Processed_footweartype-val-cropped.csv')
    parser.add_argument(
        '--f1_config',
        help='path_to_config json',
        default='config_1.json')
    parser.add_argument(
        '--f2_config',
        help='path_to_config json',
        default='config_2.json')
    args = parser.parse_args()
    args_dict = args.__dict__
    print(args_dict)
    main(args_dict)
    f = open('config.json',)
    data = json.load(f)
    #for i in data:
        #print(i, data[i])
    f.close()
