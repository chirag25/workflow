import json
import argparse
import cv2
import pandas as pd
import hashlib
import csv
# Content as a seperate class

class Workflow:
    def __init__(self, pth):
        self.config = json.load(open('config.json',))
        self.content = self.config["content"]
        if self.config["annotations"]["labels"]:
            self.labels = self.config["annotations"]["labels"]
        if self.config["annotations"]["labels"]:
            self.bbox = self.config["annotations"]["bbox"]
        if self.config["metadata"]:
            self.metadata = self.config["metadata"]

    def compute_img_hash(self, image, hashSize=8):
        image = cv2.imread(image)
        if image is not None:
            image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
            resized = cv2.resize(image, (hashSize + 1, hashSize))
            diff = resized[:, 1:] > resized[:, :-1]
            return sum([2 ** i for (i, v) in enumerate(diff.flatten()) if v])
        else:
            print("Skipping Image")
            return None

    def compute_text_hash(self, text):
        return hashlib.md5(text.encode()).hexdigest()

    def compute_file_hash(self, filename):
        BLOCK_SIZE = 65536  # The size of each read from the file
        file_hash = hashlib.sha256()  # Create the hash object
        with open(filename, 'rb') as f:  # Open the file to read it's bytes
            fb = f.read(BLOCK_SIZE)  # Read from the file. Take in the amount declared above
            while len(fb) > 0:  # While there is still data being read from the file
                file_hash.update(fb)  # Update the hash
                fb = f.read(BLOCK_SIZE)  # Read the next block from the file
        print(file_hash.hexdigest())

    def lol(self, df_lis):
        img_hash = [self.compute_img_hash(i[self.content]) for i in df_lis]
        label_hash = [self.compute_text_hash(i[self.labels]) for i in df_lis]
        hashes = {"img_hash": img_hash, "label_hash": label_hash}
        return hashes


def main(args):
    if args_dict['input']:
        if isinstance(args_dict['input'], str):
            if args_dict['input'].endswith('.csv'):
                df = pd.read_csv(args_dict['input'])
                df_lis = df.to_dict(orient='records')
            elif args_dict['input'].endswith('.json'):
                df_lis = json.load(open(args_dict['input']))
            else:
                print("input format is string and its not a csv or a json. Unsupported")
        elif isinstance(args_dict['input'], (dict, list)):
            df_lis = args_dict['input']
        else:
            print("input format is not a string and its not a csv or a json. Unsupported")

        if isinstance(df_lis, dict):
            df_lis = [df_lis]
        workflow = Workflow(args_dict['config'])
        hashes = workflow.lol(df_lis)
        for i in range(len(df_lis)):
            for hash_name in hashes:
                df_lis[i][hash_name] = hashes[hash_name][i]
        # print(df_lis)
        with open('Processed_'+str(args_dict['input']), 'w') as output_file:
            dict_writer = csv.DictWriter(output_file, df_lis[0].keys(), lineterminator = '\n')
            dict_writer.writeheader()
            dict_writer.writerows(df_lis)

        print("File Hash for processed csv is ", workflow.compute_file_hash('Processed_'+str(args_dict['input'])))
        # df = pd.DataFrame.from_dict(df_lis)
        # df.to_csv('Processed_'+str(args_dict['input']))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--input',
        help='path_to_input csv or json',
        default=None)
    parser.add_argument(
        '--config',
        help='path_to_config json',
        default='config.json')
    args = parser.parse_args()
    args_dict = args.__dict__
    print(args_dict)
    main(args_dict)
    f = open('config.json',)
    data = json.load(f)
    for i in data:
        print(i, data[i])
    f.close()
