import json
import argparse
import cv2
import pandas as pd
import hashlib
import uuid
import csv
from copy import deepcopy
import requests
from block_timer.timer import Timer as BlockTimer
from io import BytesIO
import imagehash
import time
import traceback
from PIL import Image


class image:
    def compute_hash(self, image, hash_size=16):
        url_info = dict()
        if image.startswith(('https://', 'http://', 'ftp://', 'localhost')):
            try:
                time_dict = dict()
                with BlockTimer(time_dict, 'image_download & bytes conversion'):
                    download_res = requests.get(image)
                    bin_obj = BytesIO(download_res.content)
                with BlockTimer(time_dict, 'converting_to_image'):
                    img = Image.open(deepcopy(bin_obj))
                with BlockTimer(time_dict, 'hash_calculation'):
                    ratio = int((min(img.size) * 1.0) / hash_size)
                    hash_val = str(imagehash.phash(img, hash_size=hash_size, highfreq_factor=ratio))

                '''
                with BlockTimer(time_dict, 's3_upload'):
                    image_name = str(uuid.uuid5(uuid.NAMESPACE_URL, hash_val + '_' + str(img.size[1]) + '_' + str(img.size[0]) + '_' + str(img.mode)))
                    s3_upload(bin_obj, s3_bucket,
                              os.path.join(s3_image_folder, image_name + '.jpg'),
                              acl=s3_acl_public,
                              content_type=s3_image_content_type)
                '''

                url_info[image] = dict(
                    success=True,
                    hash=hash_val,
                    height=img.size[1],
                    # s3_path=os.path.join(s3_base_url, s3_image_folder, image_name + '.jpg'),
                    width=img.size[0],
                    image_mode=img.mode,
                    time_dict=time_dict,
                    timestamp=time.time())
            except Exception as e:
                print(e)
                url_info[image] = dict(
                    success=False,
                    reason=str(e),
                    traceback=str(traceback.print_exc()),
                    timestamp=time.time())
            finally:
                return url_info[image]
        else:
            try:
                img = Image.open(image)
                ratio = int((min(img.size) * 1.0) / hash_size)
                hash_val = str(imagehash.phash(img, hash_size=hash_size, highfreq_factor=ratio))
                url_info[image] = dict(
                    success=True,
                    hash=hash_val,
                    height=img.size[1],
                    # s3_path=os.path.join(s3_base_url, s3_image_folder, image_name + '.jpg'),
                    width=img.size[0],
                    image_mode=img.mode,
                    timestamp=time.time())

            except Exception as e:
                print("Corrupt Image")
                print(e)
                url_info[image] = dict(
                    success=False,
                    reason=str(e),
                    traceback=str(traceback.print_exc()),
                    timestamp=time.time())
            finally:
                return url_info[image]


class text:
    def compute_hash(self, text):
        if text:
            return uuid.uuid5(uuid.NAMESPACE_URL, text)
        else:
            return "123456789"


class Workflow:
    def __init__(self, conf):
        self.config = conf

    def compute_file_hash(self, filename, df_lis):

        # Logic for calculating file_hash
        mapping = {}
        for i in df_lis[0].keys():
            mapping[i] = [j for j in self.config.keys() for k in range(len(self.config[j])) if (self.config[j][k]['column_name'] == i and j != 'metadata')]
        content_hash_title = [k+'_hash' for k, v in mapping.items() if v if v[0] == 'content']
        annotations_hash_title = [k+'_hash' for k, v in mapping.items() if v if v[0] == 'annotations']
        content_hash_list = [str(dict[key]) for dict in df_lis for key in dict.keys() if key in content_hash_title]
        annotations_hash_list = [str(dict[key]) for dict in df_lis for key in dict.keys() if key in annotations_hash_title]
        content_hash_list.sort()
        annotations_hash_list.sort()
        file_hash = uuid.uuid5(uuid.NAMESPACE_URL, (str(uuid.uuid5(uuid.NAMESPACE_URL, str(content_hash_list))) + str(uuid.uuid5(uuid.NAMESPACE_URL, str(annotations_hash_list)))))
        for dict in df_lis:
            dict['file_hash'] = file_hash
        '''
        # Old method for computing 'file_hash'
        with open('Processed_'+filename, 'w') as output_file:
            dict_writer = csv.DictWriter(output_file, df_lis[0].keys(), lineterminator='\n')
            dict_writer.writeheader()
            dict_writer.writerows(df_lis)

        with open('Processed_'+filename, 'r') as f:
            fb = f.read()
            file_hash = uuid.uuid5(uuid.NAMESPACE_URL, fb)
        '''
        # Saving file_hash in the Processed_ file with column name file_hash
        with open('Processed_'+filename, 'w') as output_file:
            dict_writer = csv.DictWriter(output_file, df_lis[0].keys(), lineterminator='\n')
            dict_writer.writeheader()
            dict_writer.writerows(df_lis)
        return file_hash

    def processing(self, df_lis):
        # processing function computes all kinds of hashes except 'file_hash', it also detects duplicate entries in the input
        mapping = {}
        for i in df_lis[0].keys():
            mapping[i] = [self.config[j][k]['type'] for j in self.config.keys() for k in range(len(self.config[j])) if (self.config[j][k]['column_name'] == i and j != 'metadata')]
        # Compute hashes for Content and Annotations
        for count, i in enumerate(df_lis):
            keys = deepcopy(list(i.keys()))
            for j in keys:
                if mapping[j] != []:
                    if (mapping[j][0] == 'image'):
                        img = image()
                        img_info = getattr(img, 'compute_hash')(i[j])
                        if img_info['success'] is True:
                            df_lis[count][str(j)+'_hash'] = img_info['hash']
                        else:
                            df_lis[count][str(j)+'_hash'] = "123456789"
                    else:
                        txt = text()
                        df_lis[count][str(j)+'_hash'] = getattr(txt, 'compute_hash')(i[j])
        # Compute a unique hash for every datapoint
        for count, i in enumerate(df_lis):
            keys = deepcopy(list(i.keys()))
            for j in keys:
                temp_concat = ""
                temp_content_concat = ""
                if j.endswith('_hash'):
                    temp_concat += str(i[j])
                for k in self.config["content"]:
                    for key, value in k.items():
                        if key == 'column_name':
                            temp_content_concat += str(i[value+'_hash'])
            txt = text()
            df_lis[count]['unique_hash'] = getattr(txt, 'compute_hash')(temp_concat)
            df_lis[count]['combined_content_hash'] = getattr(txt, 'compute_hash')(temp_content_concat)
        # Find and Display duplicate entries
        unique_list = [dict['unique_hash'] for dict in df_lis]
        seen = set()
        not_uniq = set()
        for x in unique_list:
            if x not in seen:
                seen.add(x)
            else:
                not_uniq.add(x)
        if not_uniq:
            print("Duplicate entries are present in input. Unique Hashes of these entries are ", list(not_uniq))

        return df_lis


def generate_config(config_pth, row):
    config = json.load(open(config_pth,))
    generated_config = {"content": [], "annotation": [], "metadata": []}
    # All the content is in data insde data_dict
    print(type(config['data_dict']['data']['input']))
    for k, v in config['data_dict']['data'].items():
        if v['type'] in ['img_path', 'image_url', 'image']:
            typ = 'image'
        else:
            typ = 'text'
        generated_config['content'].append({"column_name": v['column_name'], "type": typ})
    # Exception for column_crop
    if config['data_dict']['data']['input'].get('column_crop'):
        generated_config['annotation'].append({"column_name": config['data_dict']['data']['input']['column_crop'], "type": 'text'})
    """ For multiple label column support
    for k, v in config['labels'].items():
    """
    if config['data_dict']['labels']['type'] in ['img_path', 'image_url', 'image']:
        typ = 'image'
    else:
        typ = 'text'
    generated_config['annotation'].append({"column_name": config['data_dict']['labels']['column_name'], "type": typ})
    for key in row.keys():
        flag = 0
        for x in ['content', 'annotation']:
            for y in generated_config[x]:
                if y['column_name'] == key:
                    flag = 1
                    break
        if flag == 0:
            generated_config['metadata'].append({"column_name": key})
    print(generated_config)
    return generated_config


def check_validity_input(input):
    if isinstance(input, str):
        if input.endswith('.csv'):
            df = pd.read_csv(input)
            df_lis = df.to_dict(orient='records')
        elif input.endswith('.json'):
            df_lis = json.load(open(input))
        else:
            print("input format is string and its not a csv or a json. Unsupported")
            return 0
    elif isinstance(input, (dict, list)):
        df_lis = input
    else:
        print("input format is not a string and its not a csv or a json. Unsupported")
        return 0

    if isinstance(df_lis, dict):
        df_lis = [df_lis]
    return df_lis


def main(args):
    df_lis = check_validity_input(args_dict['input'])
    if df_lis != 0:
        generated_config = generate_config(args_dict['config'], df_lis[0])
        workflow = Workflow(generated_config)
        df_lis = workflow.processing(df_lis)
        file_hash = workflow.compute_file_hash(str(args_dict['input']), df_lis)
        print("File Hash for processed csv is ", file_hash)



if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    # INPUTS REQUIRED FOR INDEXING
    parser.add_argument(
        '--input',
        help='path_to_input csv or json',
        default='dummy.csv')
    parser.add_argument(
        '--config',
        help='path_to_config json',
        default='train_config.json')
    args = parser.parse_args()
    args_dict = args.__dict__
    print(args_dict)
    main(args_dict)
